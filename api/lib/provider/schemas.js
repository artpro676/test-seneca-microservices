'use strict';

const joi = require('joi');

const id = joi.string().trim();
const providerName = joi.string().trim().min(4).max(20);
const mcc = joi.string().trim().length(3);
const mnc = joi.string().trim().min(2).max(3);
const spn = joi.string().min(2).max(15).trim();
const imsi = joi.string().min(12).max(15).trim();
const spns = joi.array().items(spn);
const imsis = joi.array().items(imsi);

module.exports = {
    id: {
        id
    },
    create:  {
        providerName: providerName.required(),
        mcc: mcc.required(),
        mnc: mnc.required(),
        spns,
        imsis
    },
    update: {
        id,
        providerName,
        mcc,
        mnc,
        spns,
        imsis
    },
    listQuery: {
        providerName: joi.string().trim(),
        mcc: joi.string().trim(),
        mnc: joi.string().trim(),
        spn: joi.string().trim(),
        imsi: joi.string().trim()
    },
    detectQuery: {
        spn,
        imsi
    }
};
