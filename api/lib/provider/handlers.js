'use strict';

const {set, clone} = require('lodash');
const boom = require('boom');

module.exports = {
    list: function (request) {
        const query = clone(request.query);
        return request.seneca.actAsync(set(query, 'provider', 'list'));
    },
    detect: async function (request) {
        const query = clone(request.query);
        const result = await request.seneca.actAsync(set(query, 'provider', 'detect'));

        if (!result) {
            throw boom.notFound();
        }

        return result;

    },
    read: function (request) {
        return request.seneca.actAsync({
            provider: 'read',
            id: request.params.id
        });
    },
    delete: function (request) {
        return request.seneca.actAsync({
            provider: 'delete',
            id: request.params.id
        });
    },
    create: function (request) {
        const entity = request.payload;
        return request.seneca.actAsync({provider: 'create', entity});
    },
    update: function (request) {
        const entity = request.payload;
        return request.seneca.actAsync({
            provider: 'update',
            entity: set(entity, 'id', request.params.id)
        });
    }
};
