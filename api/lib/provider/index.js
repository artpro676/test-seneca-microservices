'use strict';

const provider = require('./handlers');
const schemas = require('./schemas');

module.exports = [
    {
        method: 'GET',
        path: '/provider',
        handler: provider.list,
        options: {
            validate: {
                query: schemas.listQuery
            }
        }
    },
    {
        method: 'GET',
        path: '/provider/detect',
        handler: provider.detect,
        options: {
            validate: {
                query: schemas.detectQuery
            }
        }

    },
    {
        method: 'GET',
        path: '/provider/{id}',
        handler: provider.read,
        options: {
            validate: {
                params: schemas.id
            }
        }

    },
    {
        method: 'DELETE',
        path: '/provider/{id}',
        handler: provider.delete,
        options: {
            validate: {
                params: schemas.id
            }
        }

    },
    {
        method: 'POST',
        path: '/provider',
        handler: provider.create,
        options: {
            validate: {
                payload: schemas.create
            }
        }
    },
    {
        method: 'PUT',
        path: '/provider/{id}',
        handler: provider.update,
        options: {
            validate: {
                params: schemas.id,
                payload: schemas.update
            }
        }
    }
];