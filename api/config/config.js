module.exports = {
    server: {
        port: process.env.PORT || 3000,
        host: process.env.HOST || 'localhost',
        debug: {
            request: ['error']
        },
    },
    senecaClients: {
        // define one or more clients which have to communicate with microservices based on seneca.js
        // it will mount automaticly in app.js:24
        provider: {
            port: process.env.SENECA_PROVIDER_PORT || 9002,
            host: process.env.SENECA_PROVIDER_HOST || 'localhost',
            // pin: process.env.SENECA_PROVIDER_PIN || 'provider:list,provider:read,provider:create',
        }
    }
};