'use strict';

const providerRoutes = require('../lib/provider');

const routes = [
    {
        method: 'GET',
        path: '/ping',
        handler: req => 'Ok'
    },
    ...providerRoutes
];

module.exports = server => routes.forEach(route => server.route(route));