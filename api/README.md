# API service

Contains source code of API provider, which exposes port `3000` by default :
```
http://localhost:3000
```  

##### Installation and usage:

Required node.js 8.* or higher.

Install packages :
``` 
npm install
```

Run app:
```
npm start
```

### Development

Install "nodemon" globally : 
```
npm i nodemon -g
```

Run : 
```
npm run start:development
```
 

#####API:
*       GET /provider[?providerName=v1&mcc=v2&mnc=v3&spn=v4&imsi=v5]

        Query parameters:
        - providerName (string, optional)
        - mcc (string, optional)
        - mnc (string, optional)
        - spn (string, optional)
        - imsi (string, optional)
                       
        Response:  
        [
            {
                "id": string,
                "providerName": string,
                "mcc": string,
                "mnc": string,
                "spns": [
                    string, ..
                ],
                "imsis": [
                    string, ..
                ]
            },
            ...
        ]
        
*       GET /provider/:id

        Parameters:
        - id (string, required)
        
        Response:         
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
*       PUT /provider/:id

        Parameters:
        - id (string, required)
        
        Body:
        {
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }

        Response:         
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
*       DELETE /provider/:id

        Parameters:
        - id (string, required)
                
        Response:         
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
*       POST /provider

        Body:
        {
            "providerName": string,             (required)
            "mcc": string,                      (required)
            "mnc": string,                      (required)
            "spns": [                           (required)
                string, ..
            ],
            "imsis": [                          (required)
                string, ..
            ]
        }

        Response:         
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
*       GET /provider/detect?imsi=v1&spn=v2

        Query parameters:
        - spn (string, required)
        - imsi (string, required)
                       
        Response:          
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
##### Environment variables

* `HOST` (defaults: 'localhost')
* `PORT` (defaults: 3000)
* `SENECA_PROVIDER_PORT` (defaults: 9002)
* `SENECA_PROVIDER_HOST` (defaults: 'localhost')
        
##### Dependencies : 
* boom
* chairo
* hapi
* joi
* lodash
* seneca-entity
* seneca-transport
