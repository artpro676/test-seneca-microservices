'use strict';

/**
 * Custom error wrapper to help handle statusCode by Boom (in modele Wreck (in module seneca-transport)) correctly.
 *
 * @type {module.AppError}
 */

module.exports = class AppError extends Error {
    constructor(message, statusCode = 500) {
        super(message);
        this.statusCode = statusCode;
    }
};