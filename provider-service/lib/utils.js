'use strict';

const {chain} = require('lodash');
const mncMccTable = require('../data/mcc-mnc-table');

module.exports = {
    /**
     * Copies fields defined in list "expectedFields" with values from "source" to "target".
     *
     * @param source
     * @param target
     * @param onlyIfNotEmpty
     * @param expectedFields
     * @returns {*}
     */
    copyFields: function (source, target, onlyIfNotEmpty = false, expectedFields = []) {

        expectedFields.forEach((fieldName) => {
            if (source[fieldName] || !onlyIfNotEmpty) {
                target[fieldName] = source[fieldName];
            }
        });

        return target;
    },
    /**
     * Converts IMSI into MCC & MNC.
     *
     * @param {String} imsi
     * @returns {{mcc: *, mnc: *}}
     */
    imsiToMncMcc: function (imsi) {

        if (!imsi) {
            throw new Error(`Parameter 'imsi' is required`);
        }

        const [mccmnc, mcc, sugestedMnc] = imsi.match(/^[+]?([0-9]{3})([0-9]{3})/);

        if (!mcc || !sugestedMnc) {
            throw new Error('MCC/MNC not found.');
        }

        let mncMccRecord = mncMccTable.find(item => {

            // first, filter by mcc
            if (item.mcc !== mcc.toString()) return false;

            // than compare exists mnc with suggested one
            const regex = new RegExp(item.mnc);

            return regex.test(sugestedMnc.toString());
        });

        const mnc = mncMccRecord ? mncMccRecord.mnc : sugestedMnc;

        return {mcc, mnc};
    }

};