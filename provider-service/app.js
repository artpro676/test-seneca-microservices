'use strict';

/* Prerequisites */
const seneca = require('seneca')();
const config = require('./config');

/* Plugin declaration */
seneca
    .use('basic')
    .use('entity')
    .use('mongo-store', config.mongodb)
    .use('./plugins/provider', config.plugins.provider);

