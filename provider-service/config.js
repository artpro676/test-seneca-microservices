module.exports = {
    mongodb: {
        uri: process.env.MONGO_DB_URI || 'mongodb://localhost:27017/seneca',
        options: {useNewUrlParser: true}
    },
    plugins: {
        provider: {
            port: process.env.PROVIDER_PORT || 9002
        }
    }
};