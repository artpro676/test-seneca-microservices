'use strict';

const {chain} = require('lodash');
const detectMethod = require('./detect');
const {copyFields} = require('../../lib/utils');

const QUERY_FIELDS = ['providerName', 'mcc', 'mnc', 'spn', 'imsi'];
const BODY_FIELDS = ['providerName', 'mcc', 'mnc', 'spns', 'imsis'];

/**
 * Entrypoint of plugin "provider"
 *
 * @param options
 * @returns {{name: string}}
 */
module.exports = function (options) {

    const seneca = this;
    const name = 'provider';

    const actions = [
        {pattern: `${name}:list`, handler: list},
        {pattern: `${name}:detect`, handler: detect},
        {pattern: `${name}:read`, handler: read},
        {pattern: `${name}:create`, handler: create},
        {pattern: `${name}:update`, handler: update},
        {pattern: `${name}:delete`, handler: remove},
    ];

    // configure patterns which seneca will listen
    actions.forEach(({pattern, handler}) => seneca.add(pattern, handler));

    const listenOptions = {
        port: options.port,
        pin: chain(actions).map('pattern').join().value() // should return string like that `provider:list,provider:read,...`
    };
    seneca.listen(listenOptions);

    seneca.log.info(`Plugin '${name}' listening :`, listenOptions);

    return {name};


    /**
     * Load list of entities from collection `provider`.
     *
     * @param data
     * @param reply
     */
    function list(data, reply) {

        seneca.log.info(`Query message`, data);

        const query = copyFields(data, {}, true, QUERY_FIELDS);

        seneca
            .make$(name)
            .list$(query, (err, list) => {
                if (err) return reply(err);
                seneca.log.info(`Found ${list.length} items`);
                return reply(null, list)
            });
    }

    /**
     * Detects provider by imsi and spn.
     *
     * @param data
     * @param reply
     */
    async function detect(data, reply) {

        seneca.log.info(`Query message`, data);

        return detectMethod(seneca.make$(name), data)
            .then(result => reply(null, result))
            .catch(err => {
                seneca.log.error(err);
                return reply(err);
            });
    }

    /**
     * Load one entity by id from collection 'provider'.
     *
     * @param data
     * @param reply
     */
    function read(data, reply) {

        if (!data.id) return reply('Parameter id is required.');

        seneca
            .make$(name)
            .load$({id: data.id}, (err, entity) => {
                if (err) return reply(err);
                return reply(null, entity)
            });
    }

    /**
     * Add new record to collection 'provider'.
     *
     * @param entity
     * @param reply
     */
    function create({entity}, reply) {

        if (!entity) return reply('Provider data should not be empty.');

        let provider = seneca.make$(name);

        // TODO Add validator
        provider = copyFields(entity, provider, false, BODY_FIELDS);

        provider.save$((err, result) => {
            if (err) return reply(err);
            return reply(null, result)
        });
    }

    /**
     * Update record from collection 'provider'.
     *
     * @param entity
     * @param reply
     */
    function update({entity}, reply) {

        if (!entity) return reply('Provider data should not be empty.');

        let provider = seneca.make$(name);

        // TODO Add validator
        provider.id = entity.id;
        provider = copyFields(entity, provider, true, BODY_FIELDS);

        provider.save$((err, result) => {
            if (err) return reply(err);
            return reply(null, result)
        });
    }

    /**
     * Remove record from collection 'provider'.
     *
     * @param entity
     * @param reply
     */
    function remove({id}, reply) {

        if (!id) return reply('Parameter id should be defined.');

        seneca
            .make$(name)
            .remove$({id}, (err, result) => {
                if (err) return reply(err);
                return reply(null, result)
            });
    }

};