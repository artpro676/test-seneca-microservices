# MICROSERVISES TEST PROJECT

### Project basics:

Docker-compose runs together: 

* Applications based on seneca.js tool `./provider-service`,
* Applications based on hapi.js framework, together with chario.js, seneca.js `./api` and exposes HTTP api,
* Web application bases on angular.js `./web`,
* Container with mongodb.

## Getting started

### Requirement

Before start, you need have installed :

* Linux OS
* Docker@17.05 or higher
* Docker-compose@1.11.1 or higher

### Installation and usage

Clone the repo : 
```
git clone git@bitbucket.org:artpro676/test-seneca-microservices.git
```

Then navigate to dir: 
```
cd test-seneca-microservices
```

Build images: 
```
docker-compose build --no-cache
```

Run containers: 
```
docker-compose up
```

Than open your browser and navigate to `http://localhost:8080`.

To shutdown use `Ctrl + C` in your terminal.

Environment variables is defined in `.env`

## Structure

#### API service 
###### ./api

Contains source code of API provider, which exposes port `3000` by default :
```
http://localhost:3000
```  

##### Installation and usage:

Required node.js 8.* or higher.

Install packages :
``` 
npm install
```

Run app:
```
npm start
```

### Development

Install "nodemon" globally : 
```
npm i nodemon -g
```

Run : 
```
npm run start:development
```
 

#####API:
*       GET /provider[?providerName=v1&mcc=v2&mnc=v3&spn=v4&imsi=v5]

        Query parameters:
        - providerName (string, optional)
        - mcc (string, optional)
        - mnc (string, optional)
        - spn (string, optional)
        - imsi (string, optional)
                       
        Response:  
        [
            {
                "id": string,
                "providerName": string,
                "mcc": string,
                "mnc": string,
                "spns": [
                    string, ..
                ],
                "imsis": [
                    string, ..
                ]
            },
            ...
        ]
        
*       GET /provider/:id

        Parameters:
        - id (string, required)
        
        Response:         
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
*       PUT /provider/:id

        Parameters:
        - id (string, required)
        
        Body:
        {
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }

        Response:         
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
*       DELETE /provider/:id

        Parameters:
        - id (string, required)
                
        Response:         
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
*       POST /provider

        Body:
        {
            "providerName": string,             (required)
            "mcc": string,                      (required)
            "mnc": string,                      (required)
            "spns": [                           (required)
                string, ..
            ],
            "imsis": [                          (required)
                string, ..
            ]
        }

        Response:         
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
*       GET /provider/detect?imsi=v1&spn=v2

        Query parameters:
        - spn (string, required)
        - imsi (string, required)
                       
        Response:          
        {
            "id": string,
            "providerName": string,
            "mcc": string,
            "mnc": string,
            "spns": [
                string, ..
            ],
            "imsis": [
                string, ..
            ]
        }
        
##### Environment variables

* `HOST` (defaults: 'localhost')
* `PORT` (defaults: 3000)
* `SENECA_PROVIDER_PORT` (defaults: 9002)
* `SENECA_PROVIDER_HOST` (defaults: 'localhost')
        
##### Dependencies : 
* boom
* chairo
* hapi
* joi
* lodash
* seneca-entity
* seneca-transport


#### Provider service 
###### ./provider-service

Contains source code of microservice which serves managing provider data in mongodb, and exposes port `9001` by default :
```
http://localhost:9001
```  

Service is using MCC/MNC list copied from https://github.com/musalbas/mcc-mnc-table/blob/master/mcc-mnc-table.json to split IMSI into MCC/MNC correctly upon detection modile provider.

##### Installation and usage:

Required node.js 8.* or higher.

Install packages :
```
npm install
```

Run app:
```
npm start
```

### Development

Install "nodemon" globally : 
```
npm i nodemon -g
```

Run : 
```
npm run start:development
```

##### Methods: 

*       provider:list 
        
        Example: 
            seneca.act({
                provider: 'list',
                [providerName: (string, optional),]
                [mcc: (string, optional),]
                [mnc: (string, optional),]
                [imsi: (string, optional),]
                [spn: (string, optional),]
            })
            
*       provider:detect
        
        Example: 
            seneca.act({
                provider: 'detect',
                imsi: (string),
                spn: (string)
            }) 

*       provider:read
        
        Example: 
            seneca.act({
                provider: 'read',
                id: (string)               
            }) 

*       provider:create
        
        Example: 
            seneca.act({
                provider: 'create',
                providerName: (string),
                mcc: (string),
                mnc: (string),
                imsis: (string[]),
                spns: (string[]),                                           
            }) 

*       provider:update
        
        Example: 
            seneca.act({
                provider: 'update',
                id: (string),
                providerName: (string, optional),
                mcc: (string, optional),
                mnc: (string, optional),
                imsis: (string[], optional),
                spns: (string[], optional),
            })
            
*       provider:delete

        Example: 
            seneca.act({
                provider: 'read',
                id: (string)               
            }) 

##### Environment variables

* `MONGO_DB_URI` (defaults: 'mongodb://localhost:27017/seneca')
* `PROVIDER_PORT` (defaults: 9002)

##### Dependencies 
* boom
* bluebird
* joi
* lodash
* seneca
* seneca-entity
* seneca-transport
* seneca-basic
* seneca-mongo-store

#### Web app 
###### ./web

Contains only dockerfile, which clones source code from `https://github.com/Anri1214/mobile-provider-application.git`.
Runs on port 8080 by default:
```
http://localhost:8080
```  

##### Environment variables

* `HOST` (defaults: '127.0.0.1')
* `PORT` (defaults: 8080)
* `API_URL` (defaults: 'http://localhost:3000')

